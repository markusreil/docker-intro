
docker run -t ubuntu:14.04 /bin/bash \
  -c "sleep 1 && echo Hello, I am container 1 && sleep 2" &
docker run -t ubuntu:14.04 /bin/bash \
  -c "sleep 2 && echo Hello, I am container 2 && sleep 2" &
docker run -t ubuntu:14.04 /bin/bash \
  -c "sleep 3 && echo Hello, I am container 3 && sleep 2" &
docker run -t ubuntu:14.04 /bin/bash \
  -c "sleep 4 && echo Hello, I am container 4 && sleep 2" &
